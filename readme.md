TiEmu emulates Texas Instruments calculators TI-89/92/92+/V200PLT.

It is based on XTiger, the original TI emulator for Linux, which uses the 68k emulation core from UAE (The Ultimate Amiga Emulator).
You need to either dump the ROM of your calculator (you can do so with TiLP), or get a FLASH upgrade from Texas Instrument (see the software section concerning your calculator, then download the archive and extract the file) to use this emulator. There are *no* TI ROMs provided in this package, as they are copyrighted by Texas Instruments, Inc.
However, a free (as in speech) ROM is now provided with TiEmu, namely PedRom. Try it out!
This version includes a VTI-style assembly debugger.


![](https://gitlab.com/openbsd98324/tiemu/-/raw/main/medias/large-57d62f2c855aa34d4f81c86f7edfea2b.png)

 


Non-Free:
````
https://tiroms.weebly.com/uploads/1/1/0/5/110560031/ti89.rom
````

````
https://tiroms.weebly.com/uploads/1/1/0/5/110560031/ti92p.rom
````


# Medias: Examples

![](medias/1647070946-screenshot.png)
![](medias/1647071175-screenshot.png)
![](medias/1647071222-screenshot.png)
![](medias/1647071234-screenshot.png)
![](medias/1647071846-screenshot.png)
![](medias/1647071862-screenshot.png)
![](medias/1647071886-screenshot.png)

![](medias/1647072541-screenshot.png)

# Function differentiate with 'Key F3 d(,x)'

![](medias/1648272615-screenshot.png)


# Further examples

![](medias/1652880038-screenshot.png)


# Ubuntu

![](ubuntu-tiemu-22.04.3/1724758137-screenshot.png)

# Graphs 

## Graph 1

![](medias/graph/1724759647-screenshot.png)

![](medias/graph/1724759675-screenshot.png)

## Graph 2

![](medias/graph/1724760080-screenshot.png)

![](medias/graph/1724760111-screenshot.png)

![](medias/graph/1724760173-screenshot.png)

![](medias/graph/1724760229-screenshot.png)

![](medias/graph/1724760249-screenshot.png)

![](medias/graph/1724760348-screenshot.png)



